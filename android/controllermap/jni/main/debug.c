////////////////////////////////////////////////////////////////////////////////
//
// RVGL (Generic) Copyright (c) The RV Team
//
// Desc: debug.c
// Game controller debug helpers.
//
// 2018-08-07 (Huki)
// File inception.
//
////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/stat.h>

#include "SDL.h"

#define CountOf(x) (sizeof((x)) / sizeof((x)[0]))

static char android_root_path[256];
static char dbg_log_file[256];

#ifdef __ANDROID__
static void GetAndroidPath(const char *path, char *buf);

#define DIRMODE (S_IRWXU | S_IRWXG | S_IRWXO)

#define stat(_p, _st) \
({ \
  char _buf[256]; \
  GetAndroidPath(_p, _buf); \
  stat(_buf, (_st)); \
})

#define mkdir(_p) \
({ \
  char _buf[256]; \
  GetAndroidPath(_p, _buf); \
  mkdir(_buf, DIRMODE); \
})

#define fopen(_p, _m) \
({ \
  char _buf[256]; \
  GetAndroidPath(_p, _buf); \
  fopen(_buf, _m); \
})

#define remove(_p) \
({ \
  char _buf[256]; \
  GetAndroidPath(_p, _buf); \
  remove(_buf); \
})

#define rename(_p1, _p2) \
({ \
  char _buf1[256], _buf2[256]; \
  GetAndroidPath(_p1, _buf1); \
  GetAndroidPath(_p2, _buf2); \
  rename(_buf1, _buf2); \
})

static bool CheckDirExists(const char *path)
{
  struct stat st;
  if (stat(path, &st) == -1 || !S_ISDIR(st.st_mode)) {
    return false;
  }
  return true;
}

static void FindAndroidPath(void)
{
  const char *paths[] = {
    "/sdcard",
    "/storage/sdcard0",
    "/storage/sdcard1",
    "/storage/emulated/legacy",
    "/storage/emulated/0",
    "/storage/emulated/1",
    "/storage/extSdCard"
  };

  // init storage permissions
  SDL_AndroidRequestPermission("android.permission.READ_EXTERNAL_STORAGE");
  SDL_AndroidRequestPermission("android.permission.WRITE_EXTERNAL_STORAGE");

  // check default paths
  for (int i = 0; i < CountOf(paths); ++i) {
    if (!CheckDirExists(paths[i])) {
      continue;
    }

    char buf[256];
    sprintf(buf, "%s/RVGL", paths[i]);
    if (!CheckDirExists(buf) && mkdir(buf) == -1) {
      continue;
    }

    sprintf(android_root_path, "%s/", buf);
    return;
  }

  // get app path as fallback
  const char *path = SDL_AndroidGetExternalStoragePath();
  sprintf(android_root_path, "%s/", path ?: "");
}

static void GetAndroidPath(const char *path, char *dest)
{
  if (path[0] == '/') {
    snprintf(dest, 256, "%s", path);
  } else {
    snprintf(dest, 256, "%s%s", android_root_path, path);
  }
}
#endif

static void WriteLogEntry(const char *str, ...)
{
  char buf[2048];
  va_list arglist;

  va_start(arglist, str);
  vsnprintf(buf, 2048, str, arglist);
  va_end(arglist);

  FILE *fp = fopen(dbg_log_file, "a");
  if (fp) {
    fprintf(fp, "%s", buf);
    fclose(fp);
  }
}

void LogOutputFunction(void *userdata, int category, SDL_LogPriority priority, const char *message)
{
  WriteLogEntry("%s\n", message);
}

void InitLogFile(void)
{
  #ifdef __ANDROID__
  FindAndroidPath();
  #endif

  mkdir("profiles");

  #ifdef __ANDROID__
  sprintf(dbg_log_file, "profiles/gamecontroller_log.txt");
  #else
  sprintf(dbg_log_file, "profiles/gamecontroller.log");
  #endif

  FILE *fp = fopen(dbg_log_file, "w");
  if (!fp) {
    return;
  }

  fclose(fp);

  SDL_LogSetOutputFunction(LogOutputFunction, NULL);
}

void UpdateMappingFile(const char *mapping)
{
  const char *db_file = "profiles/gamecontrollerdb.txt";
  const char *db_file_new = "profiles/gamecontrollerdb.new.txt";

  FILE *fp = fopen(db_file, "r");
  FILE *fp_new = fopen(db_file_new, "w");

  if (fp && fp_new) {
    const char *platform = SDL_GetPlatform();
    const char *platform_field = "platform:";
    char line[2048] = {};

    while (fgets(line, 2048, fp)) {
      /* Extract and verify the GUID */
      if (strncasecmp(line, mapping, 32) != 0) {
        fputs(line, fp_new);
        continue;
      }

      /* Extract and verify the platform */
      char *p = strstr(line, platform_field);
      if (!p) {
        fputs(line, fp_new);
        continue;
      }
      p += strlen(platform_field);
      char *comma = strchr(p, ',');
      if (!comma) {
        fputs(line, fp_new);
        continue;
      }
      size_t len = comma - p;
      if (strncasecmp(p, platform, len) != 0) {
        fputs(line, fp_new);
        continue;
      }
    }

    if (feof(fp)) {
      size_t len = strlen(line);
      if (len && line[len-1] != '\n') {
        fprintf(fp_new, "\n");
      }
    }

    fclose(fp);
    fp = NULL;
  }

  if (fp_new) {
    fprintf(fp_new, "%s\n", mapping);
    fclose(fp_new);
    fp_new = NULL;
  }

  remove(db_file);
  rename(db_file_new, db_file);
}
