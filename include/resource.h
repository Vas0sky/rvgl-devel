////////////////////////////////////////////////////////////////////////////////
//
// RVGL (Generic) Copyright (c) The RV Team
//
// Desc: resource.h
// Header file for resources.
//
// 2014-09-01 (Huki)
// File inception.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef __RESOURCE_H__
#define __RESOURCE_H__

////////////////////////////////////////////////////////////////////////////////
// Macros:
////////////////////////////////////////////////////////////////////////////////

/// icon
#define IDI_ICON1 101

/// version
#define VERSION_BUILD_YEAR 22
#define VERSION_BUILD_DATE 219
#define VERSION_BUILD_REV 2
#define VERSION_BUILD_PREFIX "Build"
#define VERSION_BUILD_SUFFIX "a"

#if (VERSION_BUILD_DATE < 1000)
#define VERSION_STRINGIFY2(_y, _d) #_y ".0" #_d
#else
#define VERSION_STRINGIFY2(_y, _d) #_y "." #_d
#endif  // (VERSION_BUILD_DATE < 1000)

#if (VERSION_BUILD_REV > 0)
#define VERSION_REV_STRINGIFY2(_r) VERSION_BUILD_SUFFIX "-" #_r
#else
#define VERSION_REV_STRINGIFY2(_r) VERSION_BUILD_SUFFIX
#endif  // (VERSION_BUILD_REV > 0)

#define VERSION_YEAR_STRINGIFY2(_y) "20" #_y

#define VERSION_STRINGIFY(_y, _d) VERSION_STRINGIFY2(_y, _d)
#define VERSION_REV_STRINGIFY(_r) VERSION_REV_STRINGIFY2(_r)
#define VERSION_YEAR_STRINGIFY(_y) VERSION_YEAR_STRINGIFY2(_y)

#define VERSION_BUILD VERSION_BUILD_YEAR, VERSION_BUILD_DATE
#define VERSION_BUILD_STRING \
    VERSION_STRINGIFY(VERSION_BUILD_YEAR, VERSION_BUILD_DATE) \
    VERSION_REV_STRINGIFY(VERSION_BUILD_REV)
#define VERSION_YEAR_STRING VERSION_YEAR_STRINGIFY(VERSION_BUILD_YEAR)

////////////////////////////////////////////////////////////////////////////////
// Types:
////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////
// Prototypes:
////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////
// Globals:
////////////////////////////////////////////////////////////////////////////////



#endif  // __RESOURCE_H__
