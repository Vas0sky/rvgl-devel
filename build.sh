#!/bin/bash

for i in "$@"; do
  case $i in
    --clean) clean=true;;
  esac
done

# Clean build?
if [ "$clean" = true ]; then
  rm -rf ./bin
  exit
fi

# Init build
mkdir -p bin

# Build version module
gcc -o bin/version -Iinclude version.cpp
