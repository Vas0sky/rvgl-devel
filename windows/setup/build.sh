#!/bin/bash

for i in "$@"; do
  case $i in
    --debug) debug=true;;
    --clean) clean=true;;
    --win32) win32=true;;
    --win64) win64=true;;
  esac
done

if [ "$win64" = true ]; then
  host="x86_64-w64-mingw32"
  output="rvgl_setup_win64.exe"
  dir="Win64"
  lib="lib64"
else
  host="i686-w64-mingw32"
  output="rvgl_setup_win32.exe"
  dir="Win32"
  lib="lib32"
fi

if [ "$debug" = true ]; then
  flags="-g -D_DEBUG"
  resflags="-D _DEBUG"
  target="Debug"
else
  flags="-O2 -DNDEBUG"
  resflags="-D NDEBUG"
  target="Release"
fi

objdir="obj/$target/$dir"
bindir="bin/$target/$dir"
libdir="$lib"

ccflags="-Wall -DWIN32 -D_WINDOWS -D_MBCS $flags"
ldflags="-L$libdir -static -mwindows -lgdi32 -luser32 -lkernel32 -lcomctl32 -lole32 -luuid -l7z"
resflags="-J rc -O coff $resflags"

if [ "$clean" = true ]; then
  rm -rf "$objdir"
  rm -rf "$bindir"
  exit
fi

mkdir -p "$objdir"
mkdir -p "$bindir"

prefix=`[ ! -z $host ] && echo ${host}-`
${prefix}g++ $ccflags -c main.cpp -o $objdir/main.o
${prefix}windres $resflags -i resource.rc -o $objdir/resource.res
${prefix}g++ -o $bindir/rvgl_setup.exe $objdir/main.o $objdir/resource.res $ldflags

if [ ! "$debug" = true ]; then
  ${prefix}strip $bindir/rvgl_setup.exe
  cp $bindir/rvgl_setup.exe ../../../distrib/$output
fi
