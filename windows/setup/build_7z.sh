#!/bin/bash

for i in "$@"; do
  case $i in
    --win32) win32=true;;
    --win64) win64=true;;
  esac
done

if [ "$win64" = true ]; then
  host="x86_64-w64-mingw32"
  dir="Win64"
  lib="lib64"
else
  host="i686-w64-mingw32"
  dir="Win32"
  lib="lib32"
fi

objdir="obj/Release/$dir"
libdir="$lib"

ccflags="-Wall -DWIN32 -D_WINDOWS -D_MBCS -O3 -DNDEBUG"

files_7z=(
  "7zAlloc"
  "7zBuf"
  "7zCrc"
  "7zCrcOpt"
  "7zDec"
  "7zFile"
  "7zIn"
  "7zStream"
  "Bcj2"
  "Bra"
  "Bra86"
  "CpuArch"
  "Lzma2Dec"
  "LzmaDec"
  "Ppmd7"
  "Ppmd7Dec"
)

mkdir -p "$objdir/7zip"
mkdir -p "$libdir"

prefix=`[ ! -z $host ] && echo ${host}-`
for file in ${files_7z[@]}; do
  ${prefix}gcc $ccflags -c 7zip/$file.c -o $objdir/7zip/$file.o
done
${prefix}ar crs $libdir/lib7z.a $(printf "$objdir/7zip/%s.o " "${files_7z[@]}")
