#!/bin/bash

for i in "$@"; do
  case $i in
    --debug) debug=true;;
    --clean) clean=true;;
    --win32) win32=true;;
    --win64) win64=true;;
  esac
done

if [ "$win64" = true ]; then
  host="x86_64-w64-mingw32"
  dir="Win64"
else
  host="i686-w64-mingw32"
  dir="Win32"
fi

if [ "$debug" = true ]; then
  flags="-g -D_DEBUG"
  target="Debug"
else
  flags="-O2 -DNDEBUG"
  target="Release"
fi

objdir="obj/$target/$dir"
bindir="bin/$target/$dir"

ccflags="-Wall -DWIN32 -D_WINDOWS -D_MBCS $flags"
ldflags="-static -mwindows -luser32 -lkernel32 -lole32"

if [ "$clean" = true ]; then
  rm -rf "$objdir"
  rm -rf "$bindir"
  exit
fi

mkdir -p "$objdir"
mkdir -p "$bindir"

prefix=`[ ! -z $host ] && echo ${host}-`
${prefix}g++ $ccflags -c main.cpp -o $objdir/main.o
${prefix}g++ -shared $objdir/main.o -o $bindir/dplobby_helper.dll $ldflags

if [ ! "$debug" = true ]; then
  ${prefix}strip $bindir/dplobby_helper.dll
  cp $bindir/dplobby_helper.dll ../../../distrib/bin/${dir,,}/dplobby_helper.dll
fi
